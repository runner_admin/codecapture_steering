FROM fedora:24
RUN dnf install -y curl hostname which findutils
RUN dnf -y install dnf-plugins-core
RUN dnf config-manager --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
RUN dnf config-manager --set-enabled docker-ce-edge
RUN dnf config-manager --set-disabled docker-ce-edge
RUN dnf makecache fast
RUN dnf install -y docker-ce
RUN curl -sSL https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 > /usr/local/bin/jq
RUN chmod +x /usr/local/bin/jq
RUN dnf install -y rsync
ADD . /codecapture_utils
VOLUME /codecapture_utils
