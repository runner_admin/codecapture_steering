TO="${TO:-${CI_REGISTRY_IMAGE}}"

image=$(echo $TO|awk -F: '{print $1}')
tag=$(echo $TO|awk -F: '{print $2}')

TO="$image:$(echo $tag|sed 's|/|_|g')"

echo "::: codecapture job will puth to ${TO} :::"



docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
docker inspect -f '{{json .Mounts}}' "$(hostname)-build" | jq '.[]|select(.Destination|match("/build"))' > /tmp/buildmount.json
export MOUNT=$(jq '"\(.Source):\(.Destination)"' /tmp/buildmount.json|xargs echo)
env > /tmp/build_env
cat /codecapture_utils/initialize.sh $PWD/docker.build.sh /codecapture_utils/cleanup.sh | \
     docker run -i --name "precapture-$(hostname)-build-run" --security-opt label:disable \
     -v $MOUNT -v atlas.cern.ch:/cvmfs/atlas.cern.ch \
     -e BUILDSOURCE=$PWD \
     --env-file /tmp/build_env \
     --cidfile /tmp/build.cid \
     -w /codecapture $BUILD_BASE_IMAGE \
     bash
docker commit -c 'WORKDIR /codecapture' $(cat /tmp/build.cid) "precapture-$(hostname)-build"
docker rm -f "precapture-$(hostname)-build-run"


/codecapture_utils/finalize.sh "precapture-$(hostname)-build" "${TO}" -
docker push "${TO}"
docker rmi -f "precapture-$(hostname)-build" "${TO}" || echo "removing image failed, but we don't care"
echo "::: pushed ${TO} :::"
