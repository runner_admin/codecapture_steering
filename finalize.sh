#!/bin/bash

#usage: finalize.sh preimage targetimage

echo "preimage: $1, target: $2"
PREIMAGE=$1
TARGETIMAGE=$2

cat << EOF > /tmp/finalize_dockerfile
FROM $PREIMAGE
RUN rm -rf /cvmfs #/cvmfs needs to be removed in order to allow future mounts again
WORKDIR /codecapture
EOF

cat /tmp/finalize_dockerfile|docker build -t $TARGETIMAGE -
