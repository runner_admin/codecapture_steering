#/bin/bash

source ~/.bashrc #sets ALRB env vars

echo ::: capturing analysis code :::

echo ::: Environment Variables :::
env|awk -F= '{print $1}'

echo ::: Build Source :::

ls $BUILDSOURCE

echo ::: CVMFS :::

ls -l /cvmfs/atlas.cern.ch

echo ::: Copying Build Source to $PWD :::
cp -r $BUILDSOURCE .

echo ::: Build :::

cat << EOF > ~/.ssh/config
Host *
  StrictHostKeyChecking no
Host *.cern.ch
  User $GITLABUSER
  StrictHostKeyChecking no
  GSSAPIAuthentication yes
  GSSAPIDelegateCredentials yes
  Protocol 2
  ForwardX11 no
EOF
cat ~/.ssh/config

export SVNROOT=svn+ssh://$GITLABUSER@svn.cern.ch/reps/atlasoff
export CERN_USER=$GITLABUSER


yes $GITLABPW|kinit $GITLABUSER@CERN.CH
klist

echo ::: Initialize Done :::
